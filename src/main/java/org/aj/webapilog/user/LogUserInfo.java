package org.aj.webapilog.user;



import org.aj.webapilog.config.WebApiLogProperties;

import java.util.Map;

/**
 * @author aj
 * description 获取用户信息相关接口
 * date 2023/1/27 15:50
 */
public interface LogUserInfo {

    /**
     * 获取用户id
     * @param authorization 令牌信息 key 为 {@link WebApiLogProperties#getAuthorizationKey()}
     *                      及 @RequestHeader注解里面的key
     * @return
     */
    String getUserId(Map<String,Object> authorization);

    /**
     * 获取用户账户
     * @param authorization 令牌信息 key 为 {@link WebApiLogProperties#getAuthorizationKey()}
     *                    及 @RequestHeader注解里面的key
     * @return
     */
    String getUserName(Map<String,Object> authorization);
}
