package org.aj.webapilog.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.Ordered;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @autor aj
 * description 配置打印接口日志相关属性
 * date 2022/8/6 22:28
 */
@ConfigurationProperties(prefix = "aj.web.api-log")
public class WebApiLogProperties {


    /**
     * 是否开启aop切面日志打印
     * true 或者未设置为开启 false 为不开启
     */
    private boolean enable;

    /**
     * 配置切面表达式
     */
    private String pointCutExecution;


    /**
     * 配置把解析到的请求参数放到的请求域里面对应的key
     * 这样方便下一链路获取到
     */
    private String requestAttributeRequestParamKey = "REQUEST_PARAMS";

    /**
     * 配置从请求头的哪些key 或者？请求参数里面哪些key 获取授权信息
     * 默认为 Authorization key
     */
    private String[] authorizationKey = new String[]{"Authorization"};

    /**
     * 配置此日志aop的顺序默认优先级最低
     * 一般放到最后
     */
    private Integer aopOrder = Ordered.LOWEST_PRECEDENCE;

    /**
     * 在输出请求路径（url)的时候是否包含特定的queryString
     * 有些业务模块可能是通过特定的queryString区分例如：
     * /aaa/bbb?action=query
     * /aaa/bbb?action=save
     * 即前面uri是一样的通过后面的action来区分
     * 这个时候打印请求路径的时候就需要把后面的标识打印出来方便区分
     */
    private Boolean urlBehindRequestParams = false;

    /**
     * 打印请求参数的时候需要跳过的类、
     * 默认：{@link javax.servlet.ServletRequest }
     * {@link javax.servlet.ServletResponse }
     * {@link MultipartFile }
     * {@link BindingResult}
     * 打印这些类对应的参数值是没有意义的。
     */
    private List<String> appendSkipPrintParamClass;

    /**
     * 日志模板 变量用 %{}% 进行包裹
     * 例如:
     * 访问者ip:%{visitorIp}% ....
     * 其中变量取值 仅限于 {@link org.aj.webapilog.bean.LogTemplateVariable}的常量 不属于会不解析
     * {"访问者ip":%(visitorIp)%,"访问者id":%(visitorId)%,"访问者名称":%(visitorName)%,"请求路径":%(requestUri)%,"请求媒体类型":%(contentType)%,"请求方式":%(httpMethod)%,"授权信息":%(authorization)%,"请求参数":%(requestParam)%
     * ,"返回值":%(responseResult)%,"请求开始时间":%(requestStartTime)%,"请求结束时间":%(requestEndTime)%,"结果耗时":%(cost)%,"响应状态":%(resultStatus)%}
     */
    private String logTemplate;
    /**
     * 是否启用调用开始前就打印一条请求日志 ，因为是在调用前 故是不会包含返回值 、请求耗时。
     */
    private boolean enablePrintPreLog =false;

    public String getPointCutExecution() {
        return pointCutExecution;
    }

    public void setPointCutExecution(String pointCutExecution) {
        this.pointCutExecution = pointCutExecution;
    }

    public String getRequestAttributeRequestParamKey() {
        return requestAttributeRequestParamKey;
    }

    public void setRequestAttributeRequestParamKey(String requestAttributeRequestParamKey) {
        this.requestAttributeRequestParamKey = requestAttributeRequestParamKey;
    }

    public String[] getAuthorizationKey() {
        return authorizationKey;
    }

    public void setAuthorizationKey(String[] authorizationKey) {
        this.authorizationKey = authorizationKey;
    }

    public Boolean getUrlBehindRequestParams() {
        return urlBehindRequestParams;
    }

    public void setUrlBehindRequestParams(Boolean urlBehindRequestParams) {
        this.urlBehindRequestParams = urlBehindRequestParams;
    }

    public Integer getAopOrder() {
        return aopOrder;
    }

    public void setAopOrder(Integer aopOrder) {
        this.aopOrder = aopOrder;
    }

    public List<String> getAppendSkipPrintParamClass() {
        return appendSkipPrintParamClass;
    }

    public void setAppendSkipPrintParamClass(List<String> appendSkipPrintParamClass) {
        this.appendSkipPrintParamClass = appendSkipPrintParamClass;
    }

    public String getLogTemplate() {
        return logTemplate;
    }

    public WebApiLogProperties setLogTemplate(String logTemplate) {
        this.logTemplate = logTemplate;
        return this;
    }

    public boolean isEnable() {
        return enable;
    }

    public WebApiLogProperties setEnable(boolean enable) {
        this.enable = enable;
        return this;
    }

    public boolean isEnablePrintPreLog() {
        return enablePrintPreLog;
    }

    public void setEnablePrintPreLog(boolean enablePrintPreLog) {
        this.enablePrintPreLog = enablePrintPreLog;
    }
}
