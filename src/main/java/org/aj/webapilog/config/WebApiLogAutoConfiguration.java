package org.aj.webapilog.config;

import org.aj.webapilog.aop.PrintWebLog;
import org.aj.webapilog.user.LogUserInfo;
import org.springframework.aop.aspectj.AspectJExpressionPointcutAdvisor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;

/**
 * @autor aj
 * description 配置类
 * date 2022/8/6 22:55
 */
@Configuration
@EnableConfigurationProperties(value = WebApiLogProperties.class)
@ConditionalOnWebApplication // 指明这是web应用，才会生效
@ConditionalOnProperty(name = "aj.web.api-log.enable",havingValue = "true",matchIfMissing = true)
public class WebApiLogAutoConfiguration {



    private final LogUserInfo logUserInfo;

    public WebApiLogAutoConfiguration(@Nullable LogUserInfo logUserInfo) {
        this.logUserInfo = logUserInfo;
    }


    @Bean
    public PrintWebLog printWebLog(WebApiLogProperties webApiLogProperties){
        return new PrintWebLog(webApiLogProperties,logUserInfo);
    }

    @Bean
    public AspectJExpressionPointcutAdvisor configurableAdvisor(WebApiLogProperties webApiLogProperties) {
        AspectJExpressionPointcutAdvisor advisor = new AspectJExpressionPointcutAdvisor();
        advisor.setExpression(webApiLogProperties.getPointCutExecution());
        advisor.setAdvice(printWebLog(webApiLogProperties));
        return advisor;
    }
}
