package org.aj.webapilog.bean;

/**
 * @author aj
 * description 记录模板日志变量
 * date 2023/1/25 11:41
 */
public class LogTemplateVariable {

    /**
     * 访问者ip对应的key
     */
    public static final String VISITOR_IP_KEY="visitorIp";


    private String visitorIp;

    /**
     * 访问者id
     */
    public static final String VISITOR_ID_KEY="visitorId";

    private String visitorId;

    /**
     * 访问者名称
     */
    public static final String VISITOR_NAME_KEY="visitorName";

    private String visitorName;


    /**
     * 请求地址
     */
    public static final String REQUEST_URI_KEY="requestUri";

    private String requestUri;

    /**
     * 请求方法
     */
    public static final String HTTP_METHOD_KEY="httpMethod";

    private String httpMethod;

    /**
     * 请求媒体类型
     */
    public static final String HTTP_CONTENT_TYPE_KEY="contentType";

    private String contentType;
    /**
     * 授权信息
     */
    public static final String AUTHORIZATION_KEY="authorization";

    private String authorization;

    /**
     * 请求参数
     */
    public static final String REQUEST_PARAM_KEY="requestParam";

    private String requestParam;

    /**
     * 返回值
     */
    public static final String RESPONSE_RESULT_KEY="responseResult";

    private String responseResult;

    /**
     * 响应状态码
     */
   // public static final String HTTP_STATUS_CODE="httpStatusCode";

    /**
     * 请求结果
     */
    public static final String RESULT_STATUS_KEY="resultStatus";

    private Boolean resultStatus;

    /**
     * 请求开始时间
     */
    public static final String REQUEST_START_TIME_KEY="requestStartTime";

    private String requestStartTime;

    /**
     * 请求结束时间
     */
    public static final String REQUEST_END_TIME_KEY="requestEndTime";

    private String requestEndTime;

    /**
     * 耗时
     */
    public static final String COST_KEY ="cost";

    /**
     * 耗时单位是 秒
     */
    private Double cost;

    public String getVisitorIp() {
        return visitorIp;
    }

    public void setVisitorIp(String visitorIp) {
        this.visitorIp = visitorIp;
    }

    public String getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getRequestParam() {
        return requestParam;
    }

    public void setRequestParam(String requestParam) {
        this.requestParam = requestParam;
    }

    public String getResponseResult() {
        return responseResult;
    }

    public void setResponseResult(String responseResult) {
        this.responseResult = responseResult;
    }

    public Boolean getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(Boolean resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getRequestStartTime() {
        return requestStartTime;
    }

    public void setRequestStartTime(String requestStartTime) {
        this.requestStartTime = requestStartTime;
    }

    public String getRequestEndTime() {
        return requestEndTime;
    }

    public void setRequestEndTime(String requestEndTime) {
        this.requestEndTime = requestEndTime;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }
}
